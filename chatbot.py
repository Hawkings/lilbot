#!/usr/bin/env python
# -*- coding: utf-8 -*-

import imghdr
import lilAIML as aiml
import marshal
import mgoogle
import mgoogleimage
import module
import mporn
import mtodo
import mtrans
import mwiki
import os
import re
import sys
import urllib
import uuid

noPIL = False
try:
	from PIL import Image
except ImportError:
    noPIL = True


sessionDataFile = "datos.ses"

# Ficheros ignorados por ser ineficientes:
# "aparienciaFisica.aiml"
# "estadoAnimo.aiml"
# "atributosPsicologicos.aiml" 

aimlFiles = ["adjetivos.aiml"
,"apelativos.aiml"
,"astrologia.aiml"
,"chistes.aiml"
,"conectores.aiml"
,"edad.aiml"
,"familia.aiml"
,"genero.aiml"
,"google.aiml"
,"infoBot.aiml"
,"infoUsuario.aiml"
,"insultos.aiml"
,"lilezek.aiml"
,"nacionalidades.aiml"
,"nombre.aiml"
,"nombres.aiml"
,"peticiones.aiml"
,"preguntas.aiml"
,"profesiones.aiml"
,"respuestaGenerales.aiml"
,"rule34.aiml"
,"rules.aiml"
,"saludos.aiml"
,"sino.aiml"
,"sinRespuestas.aiml"
,"todo.aiml"
,"traductor.aiml"
,"valve.aiml"
,"wiki.aiml"]

# Chapuza:
def strip_accents(s):
	return s.replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').replace('Á','A').replace('É','E').replace('Í','I').replace('Ó','O').replace('Ú','U')
	
class chatbot:
	def __init__(self):

		self.k = aiml.Kernel()
		for f in aimlFiles:
			self.k.learn(f)
		self.k.setBotPredicate("name","Cristal")
		self.k.setBotPredicate("age","17")
		self.k.setBotPredicate("gender","mujer")
		self.k.setBotPredicate("location","Murcia")
		self.k.setBotPredicate("birthday","20")
		self.k.setBotPredicate("birthmonth","Octubre")
		self.k.setBotPredicate("favoriteColor","rosa")
		self.k.setBotPredicate("footballteam","Real Betis")
		self.k.setBotPredicate("civilState","lesbiana")
		self.k.setBotPredicate("favouriteLanguage","Python")
		self.k.setBotPredicate("favoriteAuthor","Isaac Asimov")
		self.k.setBotPredicate("favoriteArtist","Chuck Norris")
		self.k.setBotPredicate("favoriteActress","Julia Roberts")
		self.k.setBotPredicate("favoriteSport","zekiball")

		self.modules = {}

	def respond(self, peer, msg):
		msg = strip_accents(msg)
		sentence = self.k.respond(msg,peer)
		session = self.k.getSessionData(peer)
		if ("system" in session.keys()):
			moduleToken = session["system"]
			self.k.setPredicate("system",None,peer)
			if (moduleToken in self.modules.keys()):
				mod = self.modules[moduleToken]
				if (mod.execute(self.k, peer, msg, sentence)):
					return sentence + mod.readOutput()
		return sentence

	def storeAllSessions(self):
		session = self.k.getSessionData()
		sessionFile = file(sessionDataFile, "wb")
		marshal.dump(session, sessionFile)
		sessionFile.close()

	def loadAllSessions(self):
		try:
			sessionFile = file(sessionDataFile, "rb")
			session = marshal.load(sessionFile)
			sessionFile.close()
		except IOError as e:
			return
		except EOFError as e:
			return 
		for k,v in session.items():
			self.loadSession(k,v)

	def loadSession(self, sesName, sesDict):
		for pred,value in sesDict.items():
			self.k.setPredicate(pred, value, sesName)
		

	def registerModule(self, mod):
		assert isinstance(mod, module.IModule)
		assert mod.getToken() not in self.modules.keys()
		self.modules[mod.getToken()] = mod

def downloadFile(url):
        filename = "./downs/"+str(uuid.uuid4())
        urllib.urlretrieve(url,filename)
        ext = imghdr.what(filename)
        if (ext is not None):
        	os.rename(filename,filename+"."+ext)
        	return filename+"."+ext
        else:
        	os.remove(filename)
        	return None

if (__name__ == "__main__"):
	tm = mtodo.moduleTodo()
	go = mgoogle.moduleGoogle()
	gi = mgoogleimage.moduleGoogleImage()
	wi = mwiki.moduleWiki()
	mt = mtrans.moduleTranslate()
	ep = mporn.modulePorn()
	bot = chatbot()
	bot.loadAllSessions()
	bot.registerModule(tm)
	bot.registerModule(go)
	bot.registerModule(gi)
	bot.registerModule(wi)
	bot.registerModule(mt)
	bot.registerModule(ep)
	line = ""
	try:
		for line in iter(sys.stdin.readline,''):
			print bot.respond("Bob",line)
			image = gi.readOutput()
			if (noPIL):
				print "<img>" + image + "</img>"
			else:
				if (image):
					img = downloadFile(image)
					if (img):
						Image.open(img).show()
					else:
						print "La imagen que estaba buscando no está disponible."
	except KeyboardInterrupt as e:
		bot.storeAllSessions()