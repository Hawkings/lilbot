#!/usr/bin/python

#http://www.eporner.com/api_xml/<tags>/<number>/<index>
#http://m.eporner.com/play/<id>

import urllib2
import getopt
import sys
import random
import string
import xml.etree.ElementTree as ET
import re

# Regular expression for web scrapping:
vid720scrap = re.compile("href\\s*=\\s*\\\"(.*)\\?start=\\\"\\s*>\\s*Download MP4 \\(720")
vid360scrap = re.compile("href\\s*=\\s*\\\"(.*)\\?start=\\\"\\s*>\\s*Download MP4 \\(360")

def epornerID(key, number):
	response = urllib2.urlopen("http://www.eporner.com/api_xml/"+key+"/1/"+str(number))
	data = response.read()
	if (len(data) < 100):
		if (number == 1):
			return None
		return epornerID(key,random.randint(1,number/10+1))
	xml = ET.XML(data)
	movies = xml.findall("./movie")
	if (len(movies) > 0):
		mid = movies[0].find("id").text
		return mid
	return None

def epornerScrap(videoID):
	response = urllib2.urlopen("http://m.eporner.com/play/"+str(videoID))
	data = response.read()
	m720 = vid720scrap.search(data)
	if (m720):
		return m720.group(1)
	m360 = vid360scrap.search(data)
	if (m360):
		return m360.group(1)
	return None


def usage(ex):
	print "Usage: %s [-i id] [-r seed] [-k key] [-h|--help] " % sys.argv[0]
	sys.exit(ex)

if __name__ == "__main__":
	keys = "all"
	srand = None
	vid = None
	try:
		opts, args = getopt.getopt(sys.argv[1:], "i:r:k:h", [])
	except getopt.GetoptError:
		usage(2)
	if (("-h","") in opts):
		usage(0)
	for arg,val in opts:
		if arg == "-r":
			try:
				srand = int(val)
			except ValueError:
				usage(2)
		if arg == "-k":
			keys = string.join(val.split(),"+")
		if arg == "-i":
			vid = val
	if (srand is not None):
		random.seed(srand)
	if (vid is None):
		n = random.randint(1,2000)
		vid = epornerID(keys,n)
	print vid and epornerScrap(vid)