#!/usr/bin/env python
# -*- coding: utf-8 -*-

import module;
import Queue;
import eporner;
import random;
import string;

#Variables a pasar:
pornTagSearch = "porn_search_terms" # varias palabras separadas por espacio
pornTagID     = "porn_tag_id"       # el id del vídeo a ver, si se conoce


class modulePorn(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        tags = None
        vid = None
        if (pornTagSearch in session.keys() and session[pornTagSearch] is not None):
            tags = session[pornTagSearch].encode('utf-8')
            tags = string.join(tags.split(),"+")
            kern.setPredicate(pornTagSearch,None,peer)
        if (pornTagID in session.keys() and session[pornTagID] is not None):
            vid = session[pornTagID].encode('utf-8')
            kern.setPredicate(pornTagID,None,peer)
        tags = tags or "all"
        vid = vid or eporner.epornerID(tags,random.randint(1,150))
        print vid
        if (vid is None):
            self.q.put("No conozco la categoría "+tags.replace("+"," "))
            return True
        self.q.put(eporner.epornerScrap(vid))
        return True


    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()


    def getToken(self):
        return "porn"
