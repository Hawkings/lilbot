#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import re
import module
import Queue
import random

# Valores a pasar del aiml:
# Términos de la búsqueda
googleImageSearchStr = "google_image_terms" # Debe ser un string
googleImageIndexStr  = "google_image_index" # Debe ser un entero no negativo

def gimage(terms, index): 
    # q : terminos
    # hl : idioma
    query = urllib.urlencode ( { 'q' : terms , 'hl' : "es", 'start' : str(index), "safe" : "off"} )
    response = urllib.urlopen ( 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0&' + query ).read()
    diccionario = json.loads(response)
    
    results = diccionario [ 'responseData' ] [ 'results' ]
    if (len(results) > 0):
        return ["",results[0]['unescapedUrl']]
    return None

class moduleGoogleImage(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        if (googleImageSearchStr in session.keys()):
            val = session[googleImageSearchStr].encode("utf-8")
            val2 = session[googleImageIndexStr].encode("utf-8")
            kern.setPredicate(googleImageSearchStr,None,peer)
            kern.setPredicate(googleImageIndexStr,None,peer)
            if (val != ""):
                try:
                    val2 = int(val2)
                except ValueError:
                    val2 = random.randint(0,20)
                r = gimage(val,val2)
                if (r):
                    self.q.put(r[0])
                    self.q.put(r[1])
                else:
                    self.q.put("No he encontrado ninguna imagen sobre ese tema")
                return True
        print "Error: google image search without query terms"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    
    def getToken(self):
        return "googleimage"
