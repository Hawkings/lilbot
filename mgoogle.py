#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import re
import module;
import Queue;

# Valores a pasar del aiml:
# Términos de la búsqueda
googleSearchStr = "google_search_terms" # Debe ser un string
googleSearchIndexStr = "google_search_index" # Debe ser un entero no negativo

# Otros parámetros
nResults = 4 # Numero de resultados por búsqueda

def google(terms, index): 
    # q : terminos
    # hl : idioma
    # start : índice
    query = urllib.urlencode ( { 'q' : terms , 'hl' : 'es', 'start' : str(index*nResults)} )
    response = urllib.urlopen ( 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&' + query ).read()
    diccionario = json.loads(response)
    
    results = diccionario [ 'responseData' ] [ 'results' ]
    if (len(results) > nResults):
        results = results[:nResults-1]
    returnval=""
    for result in results:
        title = result['title']
        url = result['unescapedUrl']
        title = re.sub(u"<[bB]>","",title)
        title = re.sub(u"</[bB]>","",title)
        returnval += title + ' ; ' + url + '\n'
    return returnval.encode('utf-8')

class moduleGoogle(module.IModule):

    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        if (googleSearchStr in session.keys()):
            val = session[googleSearchStr].encode("utf-8")
            val2 = session[googleSearchIndexStr].encode("utf-8")
            kern.setPredicate(googleSearchStr,None,peer)
            kern.setPredicate(googleSearchIndexStr,None,peer)
            if (val != ""):
                try:
                    val2 = int(val2)
                except ValueError:
                    val2 = 0
            if (val != ""):
                self.q.put(google(val,val2))
                return True
        print "Error: google search without query terms"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    
    def getToken(self):
        return "google"
